// var umur;
// if(true) {
//     var umur = 28;
// }
// function introduction(){
// }
// console.log(umur);
// console.log(introduction);


// a = lukas de markus;
// var a = sabrina;
// console.log(a);

// let turu = "dimana kita";
// console.log(turu);

// -------------------- Scope ---------//
// let turu = 450
// if(true){
//     let turu = 5000
//     console.log(turu);
// }
// console.log(turu)

// ------------ Reassigned and Redeclared -------------------------//
// let turu;
// console.log(turu)
// turu = 'dimana kita'
// console.log(turu)

// ------------ Scope ------------------------//
// const sale = 10000
// if (true){
//     const sale = 5000
//     console.log(sale)
// }
// console.log(sale)

// ----------- Reassigned and Redeclared ---------------//
// const gunung = "bulat";
// gunung = "tipis";

// const nama = 'Ahmad Ardiansyah';
// const lastname = 'Ramadhan';
// const obj = ['play games',  'hangouts',  'make a joke'];
// function sum(a, b) {
//     return (a + b) /2;
// }
// console.log(`My firstname is ${nama} and lastname is ${lastname}, My hobby are ${obj}, And my exams ${sum(70, 80)}`);

// ------------------- Percobaan Object ------------------------//
// const pelajar = {
//     nama : 'Ahmad Ardiansyah',
//     lastname : 'Ramadhan',
//     exams :{ midterm : 90, final :100}
// };
// function sum(midterm, final) {
//     return (midterm + final) /2;
// }
// const hasil = `My firstname is ${pelajar.nama} and lastname is ${pelajar.lastname}, Dan Nilai exams adalah ${sum()}`;
// console.log(hasil);

// ------------------------- Percobaan For iterasi -----------//
// const murid = [
//     {nama : "ardiansyah", jurusan:"sistem informasi"},
//     {nama : "rudiansyah", jurusan:"teknik informatika"},
//     {nama : "lariansyah", jurusan:"ilmu komputer"}
// ];
// for (let i = 0; i <murid.length; i++){
//     console.log(`My name is ${murid[i].nama} and my major is ${murid[i].jurusan}`);
// }

// ------------------------- Percobaan IF ELSE IF --------------------------------//
// let year = prompt('Tahun Berapa Ahmad Ardiansyah Ramadhan dilahirkan?');
//     if (year == 2000)  {
//     alert('Wahhhh... Kamu benar sekali')
//      }
//     else {
//         alert('Hemm kurang tepat');
//     };

// ------------------------- Percobaan Switch Case ------------------------//
// let a = 4 + 4;
// switch(a){
//     case 1:
//         console.log( 'terlalu kecil' );
//         break;
//     case 8:
//         console.log( 'Tepat Gengs ');
//         break;
//     case 10:
//         console.log('terlalu besar');
//         break;
//     default:
//         console.log("waduh apa nih");
// }

// ----------------------- Percobaan FUNCTION ES6 --------------------------------//
// function pertambahan(nilai1, nilai2){
//     return nilai1 + nilai2
// }
// const hasil = pertambahan(20,80);
// console.log(hasil);

// -------------------- ARROW FUNCTION --------------------------------//
// const pengurangan = (A, B) => A - B;
// console.log(pengurangan(20,10));

// const pelajar = {
//     nama : 'Ahmad Ardiansyah',
//     lastname : 'Ramadhan',
//     obj : ['Hangout', 'Play games', 'Make a joke sometimes'],
// }
// function sum(midterm, final) {
//     return (midterm + final) /2;
// }
// const hasil = `My firstname is ${nama} and lastname is ${lastname}, ${obj}, Dan Nilai exams adalah ${sum(80,90)}`
// console.log(hasil);

// const murid = [
//     {nama : "ardiansyah", lastname: "ramadhan", exams:{midterm= 90, final= 100}},
//     // {nama : "rudiansyah", lastname: "ramadhan", exams:{midterm= 90, final= 90}},
//     // {nama : "lariansyah", lastname: "ramadhan", exams:{midterm= 70, final= 80}}
// ];
// function mean(midterm, final) {
//     return (midterm + final)/2; 
// };
// let nilairata = mean(murid.exams.midterm, murid.exams.final);

// function sum(midterm, final) {
//     return midterm + final;
// }
// let nilaiakhir = sum(murid.exams.midterm, murid.exams.final);
// // for (let i = 0; i <murid.length; i++){
// //     console.log(`My name is ${murid[i].nama} and my lastname is ${murid[i].lastname} and my exams ${murid[i].nilairata}`);
// // }
// const hasil = `My name is ${murid.nama} and my lastname is ${murid.lastname} and my exams ${nilairata} and mean exams ${nilaiakhir}`;
// console.log(hasil);

// -------------------- FIND and Filter --------------------------------//
// const buah = ['apple', 'banana', 'grape', 'avocado', 'orange', 'apple']
// const resultWithFind = buah.find((data) => data === 'apple');
// console.log(resultWithFind);

// const resultWithFilter = buah.filter((data, index, dataOri) => data === 'apple');
// console.log(resultWithFilter);

// const murid = [
//     {nama : "ardiansyah", nilai : 90},
//     {nama : "radit", nilai: 40},
//     {nama : "bobot", nilai: 50},
//     {nama : "diana", nilai: 70},
//     {nama : "rika", nilai: 85}
// ];
// const filter = murid.filter(murid => murid.nilai >= 70);
// console.log(filter);

// ------------------- Rest Function ------------------------//
// function shwData(...data){
//     console.log(data);
// }
// shwData(2,3,4,5,6,7);
// const A = new Date();
// console.log(A);